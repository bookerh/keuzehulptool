$( document ).ready(function() {
	//animate and show the keuze-hulp tool
	$(".keuze-hulp").click(function(){
		$(".keuze-hulp-main").css({"display": "block", "position": "fixed"});
		$(".keuze-hulp-content").animate({right: '0%'}, 600);


	});
	//animate and remove the keuze-hulp-tool
	$(".close-cancel-page").click(function(){
			$(".keuze-hulp-content").animate({right: '-90%'}, 600);
			setTimeout(function(){ $(".keuze-hulp-main").fadeOut();}, 400);			
	});
//set value of the first question
var questionIndex = 1;
//show the right question (depends on user input)
showQuestion(questionIndex);
//this function shows the right question when the user go to the next question
function plusquestion(n) {
  	showQuestion(questionIndex += n);
 	//animate timeline(depends on how many questions there are)
	$(".set-timeline").animate({width: (questionIndex - 1) * (100 / $(".top-content").length) +"%"});
}
//this functions set the questionIndex to the jouw!!rrent question
function currentquestion(n) {
  showQuestion(questionIndex = n);
}
var questionShowed = 0;

questionShowed2 = 0;

var checkMark = "<svg class='check-mark-check' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 511.999 511.999'><path d='M506.231 75.508c-7.689-7.69-20.158-7.69-27.849 0l-319.21 319.211L33.617 269.163c-7.689-7.691-20.158-7.691-27.849 0-7.69 7.69-7.69 20.158 0 27.849l139.481 139.481c7.687 7.687 20.16 7.689 27.849 0l333.133-333.136c7.69-7.691 7.69-20.159 0-27.849z'/></svg>";

//this function shows the question
function showQuestion(n) {
  	var i;
  	var totalquestion = $(".top-content");

	for (i = 0; i < totalquestion.length; i++) {
      totalquestion[i].style.display = "none";  
      $(".top-content").removeClass("top-content-selected");
  	}

  	var questionIndex2 = questionIndex - 1;

  	$(".top-content:eq("+questionIndex2+")").css("display", "block").addClass("top-content-selected");

	// if the user already made a choise, enable button-left
	if($(".top-content-selected").find("li").hasClass("selected-option")){
		$('.button-right').prop("disabled", false);
	}

	//if the user is on the firstquestion, set the back button to display none;
	if(questionIndex == 1){
		$(".button-left").css("display", "none");
	}
	else{
		$(".button-left").css("display", "inline-block");
	}	
}

function showButton(){
	if($(".top-content-selected").find("div").hasClass("question-slider")){
		$('.button-right').prop("disabled", false);
		console.log("true")
	}
	else if($(".top-content-selected").find(".row").find("li").hasClass("selected-option") || $(".top-content-selected").find(".row").find("li").hasClass("selected-phone")){
		$('.button-right').prop("disabled", false);
	}
	else{
		$('.button-right').prop("disabled", true);
	}
}

function addClasses(){
	if($("#question-2").css("display") == "block"){
		$(".top-content-selected").find(".row").find("li").click(function(){
			$(this).find(".check-mark, .check-mark-check").remove();
			$(".top-content-selected").find(".row").find("li").removeClass("selected-option")
			$(this).append("<span class='check-mark'></span>" + checkMark);
			$(this).addClass("selected-option");
				showButton();
		});
	}

	if($("#question-3").css("display") == "block"){
		$(".top-content-selected").find(".row").find("li").click(function(){
			$(this).find(".check-mark, .check-mark-check").remove();
			$(".top-content-selected").find(".row").find("li").removeClass("selected-option")
			$(this).append("<span class='check-mark'></span>" + checkMark);
			$(this).addClass("selected-option");
				showButton();
		});
	}
	if($("#question-4").css("display") == "block"){
		questionShowed2 = questionShowed2 + 1;
		console.log(questionShowed2);
		if(questionShowed2 == 1){
		$(".top-content-selected").find(".row").find("li").click(function(){
			$(this).toggleClass("selected-option");
			showButton();
			if($(this).hasClass("selected-option")){
				$(this).append("<span class='check-mark'></span>" + checkMark);
			}else{
				$(this).find(this).find(".check-mark, .check-mark-check").remove();
			}
			console.log("#question-4");
		});
		}
	}
	if($("#question-5").css("display") == "block"){
		questionShowed = questionShowed + 1;
		console.log(questionShowed);
		if(questionShowed == 1){
		$(".top-content-selected").find(".row").find("li").click(function(){
			$(this).toggleClass("selected-option");
			showButton();
			if($(this).hasClass("selected-option")){
				$(this).append("<span class='check-mark'></span>" + checkMark);
			}else{
				$(this).find(this).find(".check-mark, .check-mark-check").remove();
			}
			console.log("#question-4");
		});
		}
	}

	if($('#question-6').css('display') == 'block'){
		$('#question-6').find("h1").remove();
		$("#question-6").prepend("<h1>En welke "+ $("#question-5").find(".selected-option span").text() +" telefoon heb je in gedachten?</h1>");
		$("#question-6").find("li").click(function(){
			$("li").removeClass("selected-phone");
			$(this).addClass("selected-phone");
			console.log("clicked;");
			showButton();
		});
		
	}
	
	if($("#question-7").css("display") == "block"){
		$(".top-content-selected").find(".row").find("li").click(function(){
			$(this).find(".check-mark, .check-mark-check").remove();
			$(".top-content-selected").find(".row").find("li").removeClass("selected-option")
			$(this).append("<span class='check-mark'></span>" + checkMark);
			$(this).addClass("selected-option");
				showButton();
		});
	}

	if($("#question-8").css("display") == "block"){
		$(".top-content-selected").find(".row").find("li").click(function(){
			$(this).find(".check-mark, .check-mark-check").remove();
			$(".top-content-selected").find(".row").find("li").removeClass("selected-option")
			$(this).append("<span class='check-mark'></span>" + checkMark);
			$(this).addClass("selected-option");
				showButton();
		});
		console.log("false;");
	}

	if($('#question-9').css('display') == 'block'){
		console.log("false;");
		showButton();
	}


	if($('#question-10').css('display') == 'block'){

		$('.navigation-content').css("display", "none");

		$(".main-content").css("margin-top", "0px");

		$(".first-advice").css("display", "block");

		//add the class selected-choise to the choise that the user selected
		$(".choise").click(function(){
			$('.choise').removeClass("selected-choise");
			$(this).addClass("selected-choise");
			console.log("clicked");
		});
		//show the right content with the right choise
		$(".choise-1").click(function(){
			$(".advice").css("display", "none");
			$(".first-advice").css("display", "block");
			});

		$(".choise-2").click(function(){
			$(".advice").css("display", "none");
			$(".second-advice").css("display", "block");
			});

		$(".choise-3").click(function(){
			$(".advice").css("display", "none");
			$(".third-advice").css("display", "block");
			});
		//start over call
		$(".close-content").append("<p class='reset-page' id='start-over-button'> < Start opnieuw </p>");

		//reset the keuzehulp-tool;
		$("#start-over-button").click(function(){
		plusquestion( -questionIndex + 1);
		$(".main-content").css("margin-top", "5%");
		$('.navigation-content').css("display", "block");
		$('.top-content').find('li').removeClass("selected-option");
		$('.button-right').prop("disabled", true);
		$('.top-content').find(".check-mark, svg").remove();
		$('#start-over-button').remove();
		});
		
	}
}

if($("#question-1").css("display") == "block"){
	$(".top-content-selected").find(".row").find("li").click(function(){
			$(this).find(".check-mark, .check-mark-check").remove();
			$(".top-content-selected").find(".row").find("li").removeClass("selected-option")
			$(this).append("<span class='check-mark'></span>" + checkMark);
			$(this).addClass("selected-option");
				showButton();
		});
}

//go next/prev question
$(".button-left").click(function(){

	plusquestion(-1);

	addClasses();

	if($('#question-9').css('display') == 'block'){
		$('.button-right').prop("disabled", false);
	}

});

//if a user didnt filled a answer in
$(".button-right").click(function(){

	plusquestion(1);

	addClasses();

	$('.button-right').prop("disabled", true);

	$('.keuze-hulp-content').scrollTop(0);

});


//create a slide tool at question 8
var pipsSlider = document.getElementById('slider-text-mb');

	noUiSlider.create(pipsSlider, {
	    range: {
	    	// start at 0 and end at 1000
	        min: 0,
	        max: 1000
	    },
	    //default start = 500
	    start: [ 500 ],
	    //set values that are clickable
	    pips: { mode: 'values', values: [0, 250, 500, 750, 1000] }
	});
var pips = pipsSlider.querySelectorAll('.noUi-value');

function clickOnPip ( ) {
    var value = Number(this.getAttribute('data-value'));
    pipsSlider.noUiSlider.set(value);
}

for ( var i = 0; i < pips.length; i++ ) {
    pips[i].style.cursor = 'pointer';
    pips[i].addEventListener('click', clickOnPip);
}

//style the slider the rightway
$(".noUi-marker-normal").remove();
$(".noUi-value:eq(4)").append("+</br>MB");
$(".noUi-value:eq(0)").append("</br>MB");

//owl carousel settings
$(".owl-carousel").owlCarousel({
     items : 1,
     loop  : true,
     margin : 30,
     center: true,
     nav    : true,
     dots: false,
     responsive : {
   	 768 : {
      items : 1,
   	 }
	}
   });
$(".item ul li").click(function(){
	$(this).parent().find(".toggle").removeClass(".initiallyHidden").slideToggle();
});

});
